//
//  Constants.swift
//  Assinment
//
//  Created by Rushabh on 23/05/20.
//  Copyright © 2020 Rushabh. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static let img_url = "https://image.tmdb.org/t/p/w342"
    static let back_drop = "https://image.tmdb.org/t/p/original"
    //Here we define model
    //MARK: Properties
       var movieName : String?
       var movieImage : String?
       var movieDropImage : String?
       var movieDescription : String?
       var movieDate : String?
       var popularity : Double?
    
       //MARK: Initialization
       init?(movieName : String, movieImage : String, movieDropImage : String,movieDescription : String, movieDate : String, popularity : Double) {
           // Initialize stored properties.
           self.movieName = movieName
           self.movieImage = movieImage
           self.movieDropImage = movieDropImage
           self.movieDate = movieDate
        self.movieDescription = movieDescription
        self.popularity = popularity
       }
}

//
//  TopRatedMovieListViewController.swift
//  Assinment
//
//  Created by Rushabh on 23/05/20.
//  Copyright © 2020 Rushabh. All rights reserved.
//

import UIKit

class TopRatedMovieListViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
   
    @IBOutlet weak var topRatedCollectionView: UICollectionView!
    var favouritemovieArray = NSMutableArray()
    //#PRAGMA mark -viewdidload
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        //#PRAGMA mark - fetchjsonfunction
        fetchTopMovieJSON { (res) in
                         switch res {
                         case .success(let courses):print(courses.results)
                         courses.results.forEach({ (movie) in
                           let model = Constants.init(movieName: movie.title, movieImage: movie.poster_path, movieDropImage: movie.backdrop_path, movieDescription: movie.overview, movieDate: movie.release_date, popularity: movie.popularity)
                            self.favouritemovieArray.add(model!)
                      
                             })
                         case .failure(let err):
                             print("Failed to fetch courses:", err)
                         }
                     }
    }
    // #PRAGMA mark - Api Call
            fileprivate func fetchTopMovieJSON(completion: @escaping (Result<movies, Error>) -> ()) {
                
                let urlString = "https://api.themoviedb.org/3/movie/top_rated?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
                guard let url = URL(string: urlString) else { return }
                
                URLSession.shared.dataTask(with: url) { (data, resp, err) in
                    
                    if let err = err {
                        completion(.failure(err))
                        return
                    }
                    
                    // successful
                    do {
                        let courses = try JSONDecoder().decode(movies.self, from: data!)
                        completion(.success(courses))
                        DispatchQueue.main.async {
                          
                              self.topRatedCollectionView.reloadData()
                        }
                       
      
                        
                    } catch let jsonError {
                        completion(.failure(jsonError))
       
                    }
                    
                    
                }.resume()
            }
       ////#PRAGMA mark - cell selection
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let model = favouritemovieArray[indexPath.row] as! Constants
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            controller.index = indexPath.row
            controller.movieImage = model.movieDropImage!
            controller.detailMovieArray = favouritemovieArray
           self.navigationController?.pushViewController(controller, animated: true)

       }
    ////#PRAGMA mark -collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return favouritemovieArray.count
       }
       func numberOfSections(in collectionView: UICollectionView) -> Int {
           return 1
       }
    ////#PRAGMA mark -datasource
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MoviesCollectionViewCell
           let model = favouritemovieArray[indexPath.row] as! Constants
           cell.movieName.text = model.movieName
           cell.movieDescription.text = model.movieDescription
           //Image downloading :
           let url = URL(string:"\(Constants.img_url)\(model.movieImage!)")
           print(model.movieImage as Any)
          DispatchQueue.global().async { [weak self] in
           if let data = try? Data(contentsOf: url!) {
                  if let image = UIImage(data: data) {
                      DispatchQueue.main.async {
                       cell.movieImage?.image = image
                      }
                  }
              }
          }
           
           return cell;
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MoviesCollectionViewCell.swift
//  Assinment
//
//  Created by Rushabh on 23/05/20.
//  Copyright © 2020 Rushabh. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var checkmarkLabel: UILabel!
      //#PRAGMA mark - declare bool property for editing cell
      var isInEditingMode: Bool = false {
        ////#PRAGMA mark - function to toggle bool property
          didSet {
              checkmarkLabel.isHidden = !isInEditingMode
          }
      }
      ////#PRAGMA mark - selection shows tick mark on lable and deselection remove this by using ternary operator
      override var isSelected: Bool {
          didSet {
              if isInEditingMode {
                  checkmarkLabel.text = isSelected ? "✓" : ""
              }
          }
      }
}

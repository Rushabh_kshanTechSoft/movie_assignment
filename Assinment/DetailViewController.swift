//
//  DetailViewController.swift
//  Assinment
//
//  Created by Rushabh on 23/05/20.
//  Copyright © 2020 Rushabh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var movieImage = ""
    var index: Int = 0
    var detailMovieArray = NSMutableArray()
    @IBOutlet weak var movieDetailView: UIView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDate: UILabel!
    @IBOutlet weak var movieReview: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var moviePopulrLbl: UILabel!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
   //#PRAGMA mark - viewdidload
    // set data to label from model object
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moviePopulrLbl.isHidden = true // hide label till data get loads
       //self.movieDetailView.isHidden = true // hide detailview till data get load
                DispatchQueue.main.async {
                    self.downloadImag()
                    let model = self.detailMovieArray[self.index] as! Constants

                    self.movieName.text = model.movieName!
                    self.movieDate.text = model.movieDate!
                    self.moviePopulrLbl.isHidden = false
                    self.movieDetailView.isHidden = false
                    self.movieReview.text? = String(model.popularity!)
                    self.movieDescription.text = model.movieDescription!
                  
                }
                 
               
    }
         ////#PRAGMA mark - downloading image
        func downloadImag(){
            
            //show progress indicator on screen while load data
            progressIndicator.style = .large
            progressIndicator.color = .red // show color
           //progress indicator start animating here
           self.progressIndicator.startAnimating();
            let url = URL(string:"\(Constants.back_drop)\(movieImage)")
                  DispatchQueue.global().async { [weak self] in
                    if let data = try? Data(contentsOf: (url ?? URL(string: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.simplemost.com%2Fauthor%2Fkate-streit%2Fpage%2F58%2F&psig=AOvVaw3ls5yVm_d1aDeuIElzI4YU&ust=1590389523806000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIC6xuf0y-kCFQAAAAAdAAAAABAD"))!) {
                          if let image = UIImage(data: data) {
                              DispatchQueue.main.async {
                             
                                self?.movieImg?.image = image
                                self?.progressIndicator.stopAnimating()
                              }
                          }
                        
                      }
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    }
}

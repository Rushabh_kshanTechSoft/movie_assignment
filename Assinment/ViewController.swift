//
//  ViewController.swift
//  Assinment
//
//  Created by Rushabh on 23/05/20.
//  Copyright © 2020 Rushabh. All rights reserved.
// List displayed in collection view
// Delete action will delete temporary items
// Click on cell will redirect to detail view

import UIKit
struct movies: Decodable {
    let page: Int
    let total_results: Int
    let total_pages: Int
    let results: [results]
}
struct results: Decodable {
    let title: String
    let poster_path: String
    let overview: String
    let backdrop_path: String
    let release_date: String
    let popularity: Double
}
var movieArray = NSMutableArray()
var _selectedCells = [IndexPath]()


class ViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate,UISearchBarDelegate {
       @IBOutlet weak var movieSeachBar: UISearchBar!
       @IBOutlet weak var movieCollectionView: UICollectionView!
       @IBOutlet weak var deleteButton: UIBarButtonItem!
    //#PRAGMA mark -Collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieArray.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //#PRAGMA mark -datasource
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MoviesCollectionViewCell
         cell.isInEditingMode = isEditing // set cell for editing
        let model = movieArray[indexPath.row] as! Constants // get model from constant
        //set values from model
        cell.movieName.text = model.movieName
        cell.movieDescription.text = model.movieDescription
        //Image downloading :
        let url = URL(string:"\(Constants.img_url)\(model.movieImage!)")
        print(model.movieImage as Any)
        DispatchQueue.global().async { [weak self] in
        if let data = try? Data(contentsOf: url!) {
               if let image = UIImage(data: data) {
                   DispatchQueue.main.async {
                    cell.movieImage?.image = image
                   }
               }
           }
       }
        return cell;
    }
    //#PRAGMA mark -cell deselecte action
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
                     if let selectedItems = collectionView.indexPathsForSelectedItems, selectedItems.count == 0 {
                         deleteButton.isEnabled = false
                     }
                 }
        //#PRAGMA mark -cell selection action . either to go to detail view or show delete button
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if !isEditing {
                deleteButton.isEnabled = false
                let model = movieArray[indexPath.row] as! Constants
                 
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                 controller.index = indexPath.row
                 controller.movieImage = model.movieDropImage!
                 controller.detailMovieArray = movieArray
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                deleteButton.isEnabled = true
            }
           


       }
    
  //#PRAGMA mark -Delete item butn press logic
    @IBAction func deleteItem(_ sender: Any) {
           if let selectedCells = movieCollectionView.indexPathsForSelectedItems {
               let items = selectedCells.map { $0.item }.sorted().reversed()
               for item in items {
                movieArray.remove(item)
               }
               movieCollectionView.deleteItems(at: selectedCells)
               deleteButton.isEnabled = false
           }
       }
    //#PRAGMA mark -editing method
    override func setEditing(_ editing: Bool, animated: Bool) {
             super.setEditing(editing, animated: animated)
             movieCollectionView.allowsMultipleSelection = editing
             let indexPaths = movieCollectionView.indexPathsForVisibleItems
             for indexPath in indexPaths {
                 let cell = movieCollectionView.cellForItem(at: indexPath) as! MoviesCollectionViewCell
                 cell.isInEditingMode = editing
             }
         }

   
    //#PRAGMA mark -ViewdidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // setNeedsStatusBarAppearanceUpdate()
        navigationItem.leftBarButtonItem = editButtonItem
        
        // Do any additional setup after loading the view.
        //private method call to get json response
        fetchMoviesJSON { (res) in
                  switch res {
                  case .success(let courses):print(courses.results)
                  courses.results.forEach({ (movie) in
                    let model = Constants.init(movieName: movie.title, movieImage: movie.poster_path, movieDropImage: movie.backdrop_path, movieDescription: movie.overview, movieDate: movie.release_date, popularity: movie.popularity)
                    movieArray.add(model!)
               
                      })
                  case .failure(let err):
                      print("Failed to fetch courses:", err)
                  }
              }
       
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        .darkContent
//    }
    // #PRAGMA mark - Api Call
        fileprivate func fetchMoviesJSON(completion: @escaping (Result<movies, Error>) -> ()) {
            
            let urlString = "https://api.themoviedb.org/3/movie/now_playing?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, resp, err) in
                
                if let err = err {
                    completion(.failure(err))
                    return
                }
                
                // successful
                do {
                    let courses = try JSONDecoder().decode(movies.self, from: data!)
                    completion(.success(courses))
                    DispatchQueue.main.async {
                      
                          self.movieCollectionView.reloadData()
                    }
                   
  
                    
                } catch let jsonError {
                    completion(.failure(jsonError))
   
                }
                
                
            }.resume()
        }
}

